# Using PerfMon in Athena jobs to time an algorithm

In this exercise we'll see how to enable PerfMon in Athena jobs.
Let us get started by setting up a nigthly and downloading a simple test jobOptions file:

```bash
lsetup "asetup Athena,master,latest"
get_files -jo test_perfMonSvc_cpucruncheralg.py
```

If you investigate the file, you'll see we configure an algorithm called [CpuCruncherAlg](https://gitlab.cern.ch/atlas/athena/blob/master/Control/PerformanceMonitoring/PerfMonTests/src/PerfMonTestCpuCruncherAlg.h)
as follows:

```python
from PerfMonTests.PerfMonTestsConf import PerfMonTest__CpuCruncherAlg
cpuCruncher = PerfMonTest__CpuCruncherAlg( "CpuCruncherAlg", OutputLevel = INFO )
cpuCruncher.MeanCpu = 100. # ms
cpuCruncher.RmsCpu  =   5. # ms
```

If you look at the implementation of the actual algorithm, you'll see
it simply keeps the CPU busy with some math operations for a given amount of 
time that can fluctuate around a mean that are setup with `RmsCpu` and `MeanCpu`, respectively.

Now let's run this jobOptions by first enabling the `fastmon` and then `sdmonfp`:

```bash
athena [--threads=1] --pmon=fastmon test_perfMonSvc_cpucruncheralg.py
```

If all goes well, you should have an output that looks like this (towards the end):

```
Py:PerfMonSvc        INFO Statistics for 'evt': (nbr entries = 99) 
Py:PerfMonSvc        INFO <cpu>:      (    100.202 +/-      0.571 ) ms
Py:PerfMonSvc        INFO <cpu_user>: (    100.101 +/-      0.562 ) ms
Py:PerfMonSvc        INFO <cpu_sys>:  (      0.101 +/-      0.100 ) ms
Py:PerfMonSvc        INFO <real>:     (    100.848 +/-     12.572 ) ms
Py:PerfMonSvc        INFO <vmem>:     (    812.242 +/-      0.000 ) MB
Py:PerfMonSvc        INFO <malloc>:   (      0.000 +/-      0.000 ) MB
Py:PerfMonSvc        INFO <nalloc>:   (      0.000 +/-      0.000 ) calls
Py:PerfMonSvc        INFO <rt>:       (    100.273 +/-      0.485 ) ns
```

Here you can see that the average `cpu` time in the event-loop excluding the first event is
`100.202 +/- 0.571 ms` per event. The first value is essentially the input `MeanCpu` 
while the second is the sampling uncertainty, i.e. `RmsCpu/sqrt(N)` where `N` is the number of events,
which in this case is expected to be `0.5 ms`. Not bad. However, this is for the 
total event but what happens if there are multiple components that are running per event and 
we want to get the measurements for each. Here comes the [PerfMonSD](https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/PerfMonSD) which you can enable with:

```bash
athena [--threads=1] --pmon=sdmonfp test_perfMonSvc_cpucruncheralg.py
```

If you run this way, you should see lines printed like:

```
PMonSD ======================================= step evt =======================================
PMonSD          n    cpu    max@evt    vmem    max@evt  malloc    max@evt  component
PMonSD [evt]   99    101    110@10        0      0@0         0      0@0    CpuCruncherAlg
...
```

Here you can see that our `CpuCruncherAlg` has an average `cpu` time of `101 ms` with the 
maximum of `110 ms` in the `10th` event. You can also find the information related to 
`vmem` and `malloc` information per component in this case.

Towards the bottom you should also see something that looks similar to:

```
PMonSD ===================================== special info =====================================
PMonSD          n      cpu     wall     vmem   malloc component
PMonSD [---]  100      102      102        -        - evtloop_time
PMonSD [---]    1      538      755        -        - overhead_time 
PMonSD [---]    1        0        0   796600   295141 snapshot_pre_ini
PMonSD [---]    1      530      749   831736   316788 snapshot_post_ini
PMonSD [---]    1      630      851   831736   316828 snapshot_post_1stevt
PMonSD [---]    1    10730    10954   831752   316925 snapshot_post_lastevt
PMonSD [---]    1    10740    10960   831752   316838 snapshot_post_fin
PMonSD [---]   91        -        -        0        1 leakperevt_evt11to100
PMonSD [---]    0        -        -        0        0 leakperevt_evt101plus
PMonSD [---] vmem_peak=831756 vmem_mean=831736 rss_mean=446508
PMonSD [---] jobcfg_walltime=9771 jobstart=2019-09-20T09:56:04+0000 
PMonSD [---] cpu_bmips=6624 cpu_res=10 release=22.0.5/x86_64-centos7-gcc8-opt
PMonSD [---] cpu_model=Intel(R)_Xeon(R)_CPU_E5-2667_v2_@_3.30GHz/25600_KB
PMonSD [---] malloc=libtcmalloc_minimal.so/ok pycintex_vmemfix=0
PMonSD [---] pmonsd_cost_onceperjob=0 pmonsd_cost_perevt=0  
PMonSD =========================== semi-detailed perfmon info / end ===========================
```

This block summarizes the information for the so-called `snapshots`, i.e. the average `cpu`/`wall`-time per event,
the `cpu`/`wall`-time as well as `vmem` and `malloc` usages at certain points of the job as well as the 
estimated `vmem` slope (i.e. leak) in the events between `10th-100th` and `101th+`. In addition, 
you can also find some extra useful information.

You can change the algorithm configurations i.e. `MeanCpu` and `RmsCpu`, and see how the results change.

In both cases, you should also have a file named `ntuple.pmon.gz` in the run folder. 
This file holds the same information (and a bit more).
